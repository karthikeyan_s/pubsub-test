/**
 * Created by ksubbaraj on 10/28/14.
 */
public class MessagePersistService implements IObserver<MessageQueue.Message> {

    private ISubject<MessageQueue.Message> messageSubject;

    public MessagePersistService(ISubject<MessageQueue.Message> messageISubject) {
        this.messageSubject = messageISubject;
        this.messageSubject.register(this);
    }

    @Override
    public void update() {
        MessageQueue.Message newMessage = messageSubject.getData();
        persist(newMessage.getMessage());
    }

    private void persist(String newMessage) {
        // Simulate Message persistence.

        System.out.println("Persisted the message: " + newMessage);
    }
}
