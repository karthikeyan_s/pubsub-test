/**
 * Created by ksubbaraj on 10/28/14.
 */
public class MessageLoggingService implements IObserver<MessageQueue.Message> {

    private final ISubject<MessageQueue.Message> messageSubject;

    public MessageLoggingService(ISubject<MessageQueue.Message> messageSubject) {
        this.messageSubject = messageSubject;
        messageSubject.register(this);
    }

    @Override
    public void update() {
        MessageQueue.Message data = messageSubject.getData();
        logMessage(data.getMessage());
    }

    private void logMessage(String data) {
        // Simulate Logging here ..
        System.out.println("Logged the message, " + data);
    }
}
