import java.util.Date;

/**
 * Created by ksubbaraj on 10/28/14.
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        MessageQueue queue = MessageQueue.INSTANCE;

        MessagePersistService persistService = new MessagePersistService(queue);
        MessageLoggingService loggingService = new MessageLoggingService(queue);

        queue.enqueue(new MessageQueue.Message("Hello", new Date(), "TestAuthor"));
        queue.enqueue(new MessageQueue.Message("World", new Date(), "TestAuthor"));

        loggingService = null;
        System.gc();
        Thread.sleep(1000);
        queue.enqueue(new MessageQueue.Message("Only persist expected", new Date(), "TestAuthor"));
        queue.enqueue(new MessageQueue.Message("Only persist expected second time", new Date(), "TestAuthor"));
    }
}
