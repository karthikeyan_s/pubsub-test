import java.lang.ref.WeakReference;
import java.util.*;

/**
 * Created by ksubbaraj on 10/28/14.
 */
public enum MessageQueue implements ISubject<MessageQueue.Message> {
    INSTANCE;

    private List<WeakReference<IObserver>> observers = new LinkedList<WeakReference<IObserver>>();
    private Message message;
    private Queue<Message> queue = new ArrayDeque<Message>();

    @Override
    public void register(IObserver observer) {
        observers.add(new WeakReference<IObserver>(observer));
    }

    @Override
    public void unregister(IObserver observer) {
        observers.remove(observer);
    }

    @Override
    public void notifyObservers() {
        for(WeakReference<IObserver> observer : observers) {
            IObserver iObserver = observer.get();
            if(iObserver != null) {
                iObserver.update();
            } else {
                System.out.println("Removed cleared observer");
                observers.remove(observer);
            }
        }
    }

    @Override
    public Message getData() {
        return this.message;
    }

    public void enqueue(Message message) {
        queue.add(message);
        this.message = message;
        notifyObservers();
    }

    public static class Message {
        private final String message;
        private final Date postedAt;
        private final String authorName;

        public String getMessage() {
            return message;
        }

        public Date getPostedAt() {
            return postedAt;
        }

        public String getAuthorName() {
            return authorName;
        }

        public Message(String message, Date postedAt, String authorName) {
            this.message = message;
            this.postedAt = postedAt;
            this.authorName = authorName;
        }
    }
}
