/**
 * Created by ksubbaraj on 10/28/14.
 */
public interface ISubject<T> {

    void register(IObserver observer);
    void unregister(IObserver observer);

    void notifyObservers();

    T getData();
}
