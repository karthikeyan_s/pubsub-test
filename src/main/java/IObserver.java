/**
 * Created by ksubbaraj on 10/28/14.
 */
public interface IObserver<T> {

    void update();
}
